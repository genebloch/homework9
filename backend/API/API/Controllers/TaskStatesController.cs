﻿using BLL.Interfaces;
using DTO;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskStatesController : ControllerBase
    {
        private readonly IService<TaskStateDTO> _statesService;

        public TaskStatesController(IService<TaskStateDTO> statesService)
        {
            _statesService = statesService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            var states = await _statesService.GetAllAsync();

            return Ok(states);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            var state = await _statesService.GetAsync(id);

            if (state == null) return NotFound(state);

            return Ok(state);
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] TaskStateDTO state)
        {
            var created = await _statesService.CreateAsync(state);

            if (!created) return BadRequest(state);

            return Created($"{state.Id}", state);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateAsync([FromBody] TaskStateDTO state)
        {
            var updated = await _statesService.UpdateAsync(state);

            if (!updated) return BadRequest(state);

            return Ok(state);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            var deleted = await _statesService.DeleteAsync(id);

            if (!deleted) return NotFound();

            return NoContent();
        }
    }
}

﻿using BLL.Interfaces;
using DTO;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IService<UserDTO> _usersService;

        public UsersController(IService<UserDTO> usersService)
        {
            _usersService = usersService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            var users = await _usersService.GetAllAsync();

            return Ok(users);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            var user = await _usersService.GetAsync(id);

            if (user == null) return NotFound(user);

            return Ok(user);
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] UserDTO user)
        {
            var created = await _usersService.CreateAsync(user);

            if (!created) return BadRequest(user);

            return Created($"{user.Id}", user);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateAsync([FromBody] UserDTO user)
        {
            var updated = await _usersService.UpdateAsync(user);

            if (!updated) return BadRequest(user);

            return Ok(user);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            var deleted = await _usersService.DeleteAsync(id);

            if (!deleted) return NotFound();

            return NoContent();
        }
    }
}

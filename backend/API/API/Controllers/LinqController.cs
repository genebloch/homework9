﻿using BLL.Services;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinqController : ControllerBase
    {
        private readonly LinqService _linqService;

        public LinqController(LinqService linqService)
        {
            _linqService = linqService;
        }

        [HttpGet("UserTasksInProject/{id}")]
        public async Task<IActionResult> GetUserTasksInProjectAsync(int id)
        {
            var output = await _linqService.GetUserTasksInProjectAsync(id);

            if (output == null) return NotFound(output);

            return Ok((output.Values, output.Keys));
        }

        [HttpGet("UserTasksWithRestrictedNameLength/{id}")]
        public async Task<IActionResult> GetUserTasksWithRestrictedNameLengthAsync(int id)
        {
            var tasks = await _linqService.GetUserTasksWithRestrictedNameLengthAsync(id);

            if (tasks == null) return NotFound(tasks);

            return Ok(tasks);
        }

        [HttpGet("UserFinishedTasksIn2020/{id}")]
        public async Task<IActionResult> GetUserFinishedTasksIn2020Async(int id)
        {
            var output = await _linqService.GetUserFinishedTasksIn2020Async(id);

            if (output == null) return NotFound(output);

            return Ok(output);
        }

        [HttpGet("UsersOlderThan10Years")]
        public async Task<IActionResult> GetUsersOlderThan10YearsAsync()
        {
            var users = await _linqService.GetUsersOlderThan10YearsAsync();

            return Ok(users);
        }

        [HttpGet("SortedUsersWithTasks")]
        public async Task<IActionResult> GetSortedUsersWithTasksAsync()
        {
            var output = await _linqService.GetSortedUsersWithTasksAsync();

            return Ok(output);
        }

        [HttpGet("UserInfo/{id}")]
        public async Task<IActionResult> GetUserInfoAsync(int id)
        {
            var info = await _linqService.GetUserInfoAsync(id);

            if (info == null) return NotFound(info);

            return Ok(info);
        }

        [HttpGet("ProjectsInfo")]
        public async Task<IActionResult> GetProjectsInfoAsync()
        {
            var info = await _linqService.GetProjectsInfoAsync();

            return Ok(info);
        }

        [HttpGet("UserNotFinishedTasks/{id}")]
        public async Task<IActionResult> GetUserNotFinishedTasksAsync(int id)
        {
            var tasks = await _linqService.GetUserNotFinishedTasksAsync(id);

            if (tasks.Count() == 0) return NotFound(tasks);

            return Ok(tasks);
        }
    }
}

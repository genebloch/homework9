﻿using BLL.Interfaces;
using DTO;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly IService<TeamDTO> _teamsService;

        public TeamsController(IService<TeamDTO> teamsService)
        {
            _teamsService = teamsService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            var teams = await _teamsService.GetAllAsync();

            return Ok(teams);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            var team = await _teamsService.GetAsync(id);

            if (team == null) return NotFound(team);

            return Ok(team);
        }

        [HttpPost]
        public async Task<IActionResult> CreateAsync([FromBody] TeamDTO team)
        {
            var created = await _teamsService.CreateAsync(team);

            if (!created) return BadRequest(team);

            return Created($"{team.Id}", team);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateAsync([FromBody] TeamDTO team)
        {
            var updated = await _teamsService.UpdateAsync(team);

            if (!updated) return BadRequest(team);

            return Ok(team);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            var deleted = await _teamsService.DeleteAsync(id);

            if (!deleted) return NotFound();

            return NoContent();
        }
    }
}

﻿using BLL.Interfaces;
using DAL.Interfaces;
using DTO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class UsersService : IService<UserDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly AutoMapper.IMapper _mapper;

        public UsersService(IUnitOfWork unitOfWork, AutoMapper.IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<bool> CreateAsync(UserDTO item)
        {
            var mapped = _mapper.Map<DAL.Models.User>(item);

            var results = new List<ValidationResult>();
            var context = new ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true)) return false;

            await _unitOfWork.Users.CreateAsync(mapped);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var status = await _unitOfWork.Users.DeleteAsync(id);

            await _unitOfWork.SaveChangesAsync();

            return status;
        }

        public async Task<UserDTO> GetAsync(int id)
        {
            var user = await _unitOfWork.Users.GetAsync(id);

            return _mapper.Map<UserDTO>(user);
        }

        public async Task<IEnumerable<UserDTO>> GetAllAsync()
        {
            var users = await _unitOfWork.Users.GetAllAsync();

            return _mapper.Map<IEnumerable<UserDTO>>(users);
        }

        public async Task<bool> UpdateAsync(UserDTO item)
        {
            var mapped = _mapper.Map<DAL.Models.User>(item);

            var results = new List<ValidationResult>();
            var context = new ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true)) return false;

            await _unitOfWork.Users.UpdateAsync(mapped);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }
    }
}

﻿using BLL.Interfaces;
using DAL.Interfaces;
using DTO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class ProjectsService : IService<ProjectDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly AutoMapper.IMapper _mapper;

        public ProjectsService(IUnitOfWork unitOfWork, AutoMapper.IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<bool> CreateAsync(ProjectDTO item)
        {
            var mapped = _mapper.Map<DAL.Models.Project>(item);

            var results = new List<ValidationResult>();
            var context = new ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true)) return false;

            await _unitOfWork.Projects.CreateAsync(mapped);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var status = await _unitOfWork.Projects.DeleteAsync(id);

            await _unitOfWork.SaveChangesAsync();

            return status;
        }

        public async Task<ProjectDTO> GetAsync(int id)
        {
            var project = await _unitOfWork.Projects.GetAsync(id);

            return _mapper.Map<ProjectDTO>(project);
        }

        public async Task<IEnumerable<ProjectDTO>> GetAllAsync()
        {
            var projects = await _unitOfWork.Projects.GetAllAsync();

            return _mapper.Map<IEnumerable<ProjectDTO>>(projects);
        }

        public async Task<bool> UpdateAsync(ProjectDTO item)
        {
            var mapped = _mapper.Map<DAL.Models.Project>(item);

            var results = new List<ValidationResult>();
            var context = new ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true)) return false;
            
            await _unitOfWork.Projects.UpdateAsync(mapped);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }
    }
}

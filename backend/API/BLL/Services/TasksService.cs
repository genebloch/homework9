﻿using BLL.Interfaces;
using DAL.Interfaces;
using DTO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class TasksService : IService<TaskDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly AutoMapper.IMapper _mapper;

        public TasksService(IUnitOfWork unitOfWork, AutoMapper.IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<bool> CreateAsync(TaskDTO item)
        {
            var mapped = _mapper.Map<DAL.Models.Task>(item);

            var results = new List<ValidationResult>();
            var context = new ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true)) return false;

            await _unitOfWork.Tasks.CreateAsync(mapped);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var status = await _unitOfWork.Tasks.DeleteAsync(id);

            await _unitOfWork.SaveChangesAsync();

            return status;
        }

        public async Task<TaskDTO> GetAsync(int id)
        {
            var task = await _unitOfWork.Tasks.GetAsync(id);

            return _mapper.Map<TaskDTO>(task);
        }

        public async Task<IEnumerable<TaskDTO>> GetAllAsync()
        {
            var tasks = await _unitOfWork.Tasks.GetAllAsync();

            return _mapper.Map<IEnumerable<TaskDTO>>(tasks);
        }

        public async Task<bool> UpdateAsync(TaskDTO item)
        {
            var mapped = _mapper.Map<DAL.Models.Task>(item);

            var results = new List<ValidationResult>();
            var context = new ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true)) return false;

            await _unitOfWork.Tasks.UpdateAsync(mapped);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }
    }
}

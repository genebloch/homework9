﻿using BLL.Interfaces;
using DAL.Interfaces;
using DTO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class TaskStatesService : IService<TaskStateDTO>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly AutoMapper.IMapper _mapper;

        public TaskStatesService(IUnitOfWork unitOfWork, AutoMapper.IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<bool> CreateAsync(TaskStateDTO item)
        {
            var mapped = _mapper.Map<DAL.Models.TaskState>(item);

            var results = new List<ValidationResult>();
            var context = new ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true)) return false;

            await _unitOfWork.States.CreateAsync(mapped);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var status = await _unitOfWork.States.DeleteAsync(id);

            await _unitOfWork.SaveChangesAsync();

            return status;
        }

        public async Task<TaskStateDTO> GetAsync(int id)
        {
            var state = await _unitOfWork.States.GetAsync(id);

            return _mapper.Map<TaskStateDTO>(state);
        }

        public async Task<IEnumerable<TaskStateDTO>> GetAllAsync()
        {
            var states = await _unitOfWork.States.GetAllAsync();

            return _mapper.Map<IEnumerable<TaskStateDTO>>(states);
        }

        public async Task<bool> UpdateAsync(TaskStateDTO item)
        {
            var mapped = _mapper.Map<DAL.Models.TaskState>(item);

            var results = new List<ValidationResult>();
            var context = new ValidationContext(mapped);

            if (!Validator.TryValidateObject(mapped, context, results, true)) return false;

            await _unitOfWork.States.UpdateAsync(mapped);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }
    }
}

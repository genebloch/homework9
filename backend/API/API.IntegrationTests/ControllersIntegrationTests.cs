using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using System.Linq;
using System;

namespace API.IntegrationTests
{
    public class ControllersIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private static string _base;
        private readonly HttpClient _client;

        public ControllersIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _base = "https://localhost:44354/api";
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task CreateProject_WhenModelIsCorrect_ThanResponseCode201()
        {
            var model = new DAL.Models.Project()
            {
                Name = "Test project",
                Description = "Desctiption desctiption desctiption",
                AuthorId = 2,
                TeamId = 3
            };
            var json = JsonConvert.SerializeObject(model);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await _client.PostAsync($"{_base}/Projects", content);

            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task CreateProject_WhenModelIsInvalid_ThanResponseCode400()
        {
            var model = new DAL.Models.Project();
            var json = JsonConvert.SerializeObject(model);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await _client.PostAsync($"{_base}/Projects", content);

            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task DeleteUser_WhenUserExist_ThanResponseCode204()
        {
            var response = await _client.DeleteAsync($"{_base}/Users/10");

            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
        }

        [Fact]
        public async Task DeleteUser_WhenUserNotExist_ThanResponseCode404()
        {
            var response = await _client.DeleteAsync($"{_base}/Users/1205");

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task CreateTeam_WhenModelIsCorrect_ThanResponseCode201()
        {
            var model = new DAL.Models.Team()
            {
                Name = "Name"
            };
            var json = JsonConvert.SerializeObject(model);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await _client.PostAsync($"{_base}/Teams", content);

            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        [Fact]
        public async Task CreateTeam_WhenModelIsInvalid_ThanResponseCode400()
        {
            var model = new DAL.Models.Team();
            var json = JsonConvert.SerializeObject(model);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await _client.PostAsync($"{_base}/Teams", content);

            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task DeleteTask_WhenTaskExist_ThanResponseCode204()
        {
            var response = await _client.DeleteAsync($"{_base}/Tasks/15");

            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
        }

        [Fact]
        public async Task DeleteTask_WhenTaskNotExist_ThanResponseCode404()
        {
            var response = await _client.DeleteAsync($"{_base}/Tasks/3015");

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetUsersOlderThan10YearsSortedAndGrouped_ThanCorrectListMustBeReturned()
        {
            var linqContent = _client.GetAsync($"{_base}/Linq/UsersOlderThan10Years").Result.Content;
            var linqJson = await linqContent.ReadAsStringAsync();
            var actualList = JsonConvert.DeserializeObject<List<DAL.Models.User>>(linqJson);

            var usersContent = _client.GetAsync($"{_base}/Users").Result.Content;
            var usersJson = await usersContent.ReadAsStringAsync();
            var usersList = JsonConvert.DeserializeObject<List<DAL.Models.User>>(usersJson);
            var expectedList = usersList.Where(u => DateTime.Now.Year - u.Birthday.Year > 10)
                .OrderByDescending(u => u.RegisteredAt)
                .GroupBy(u => u.TeamId)
                .SelectMany(u => u).ToList();

            bool equal = true;
            int i = 0;
            while (i < actualList.Count)
            {
                if (actualList[i].Id != expectedList[i].Id)
                {
                    equal = false;
                    break;
                }

                i++;
            }

            Assert.Equal(expectedList.Count, actualList.Count);
            Assert.True(equal);
        }

        [Theory]
        [InlineData(2)]
        [InlineData(10)]
        [InlineData(15)]
        [InlineData(20)]
        [InlineData(23)]
        public async Task GetUserNotFinishedTasks_WhenUserExist_ThanReturnCorrectList(int userId)
        {
            var linqContent = _client.GetAsync($"{_base}/Linq/UserNotFinishedTasks/{userId}").Result.Content;
            var linqJson = await linqContent.ReadAsStringAsync();
            var actualList = JsonConvert.DeserializeObject<List<DAL.Models.Task>>(linqJson);

            var tasksContent = _client.GetAsync($"{_base}/Tasks").Result.Content;
            var tasksJson = await tasksContent.ReadAsStringAsync();
            var tasksList = JsonConvert.DeserializeObject<List<DAL.Models.Task>>(tasksJson);
            var expectedList = tasksList.Where(t => t.PerformerId == userId && t.State != 3).ToList();

            bool equal = true;
            int i = 0;
            while (i < actualList.Count)
            {
                if (actualList[i].Id != expectedList[i].Id)
                {
                    equal = false;
                    break;
                }

                i++;
            }

            Assert.Equal(expectedList.Count, actualList.Count);
            Assert.True(equal);
        }

        [Theory]
        [InlineData(205)]
        [InlineData(215)]
        [InlineData(216)]
        [InlineData(300)]
        [InlineData(2015)]
        public async Task GetUserNotFinishedTasks_WhenUserNotExist_ThanResponseCode404(int userId)
        {
            var response = await _client.GetAsync($"{_base}/Linq/UserNotFinishedTasks/{userId}");

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }
    }
}

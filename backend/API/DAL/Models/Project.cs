﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DAL.Models
{
    public class Project
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [MinLength(15)]
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        [Range(1, int.MaxValue)]
        public int AuthorId { get; set; }
        [Range(1, int.MaxValue)]
        public int TeamId { get; set; }
    }
}

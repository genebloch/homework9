﻿using System.Reflection;
using System.Collections.Generic;
using System.IO;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace DAL.Context
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            var users = Get<User>("Users");
            var teams = Get<Team>("Teams");
            var projects = Get<Project>("Projects");
            var tasks = Get<Task>("Tasks");
            var states = Get<TaskState>("TaskStates");

            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(tasks);
            modelBuilder.Entity<TaskState>().HasData(states);
        }

        private static List<T> Get<T>(string model)
        {
            List<T> output = null;

            string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            using (StreamReader reader = new StreamReader($"{path}../../../../../seed/{model}.json"))
            {
                string json = reader.ReadToEnd();

                output = JsonConvert.DeserializeObject<List<T>>(json);
            }

            return output;
        }
    }
}

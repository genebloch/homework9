﻿using DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace DAL.Context
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<User> Users { get; private set; }
        public DbSet<Team> Teams { get; private set; }
        public DbSet<Project> Projects { get; private set; }
        public DbSet<Task> Tasks { get; private set; }
        public DbSet<TaskState> States { get; private set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Seed();
        }
    }
}

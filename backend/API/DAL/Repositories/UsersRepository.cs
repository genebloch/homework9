﻿using DAL.Context;
using DAL.Interfaces;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class UsersRepository : IRepository<Models.User>
    {
        private readonly DataContext _context;

        public UsersRepository() { }

        public UsersRepository(DataContext context)
        {
            _context = context;
        }

        public virtual async Task CreateAsync(Models.User item) => await _context.Users.AddAsync(item);

        public async Task<bool> DeleteAsync(int id)
        {
            var item = await _context.Users.FirstOrDefaultAsync(u => u.Id == id);

            if (item == null) return false;

            _context.Users.Remove(item);
            return true;
        }

        public async Task<Models.User> GetAsync(int id) => await _context.Users.FirstOrDefaultAsync(u => u.Id == id);
        public virtual async Task<IEnumerable<Models.User>> GetAllAsync() => await _context.Users.ToListAsync();

        public virtual async Task<bool> UpdateAsync(Models.User item)
        {
            var exists = await _context.Users.ContainsAsync(item);

            if (!exists) return false;

            _context.Entry(item).State = EntityState.Modified;
            return true;
        }
    }
}

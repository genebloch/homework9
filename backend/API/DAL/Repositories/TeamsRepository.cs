﻿using DAL.Context;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class TeamsRepository : IRepository<Models.Team>
    {
        private readonly DataContext _context;

        public TeamsRepository() { }

        public TeamsRepository(DataContext context)
        {
            _context = context;
        }

        public async Task CreateAsync(Models.Team item) => await _context.Teams.AddAsync(item);

        public async Task<bool> DeleteAsync(int id)
        {
            var item = await _context.Teams.FirstOrDefaultAsync(t => t.Id == id);

            if (item == null) return false;

            _context.Teams.Remove(item);
            return true;
        }

        public async Task<Models.Team> GetAsync(int id) => await _context.Teams.FirstOrDefaultAsync(t => t.Id == id);
        public virtual async Task<IEnumerable<Models.Team>> GetAllAsync() => await _context.Teams.ToListAsync();

        public async Task<bool> UpdateAsync(Models.Team item)
        {
            var exists = await _context.Teams.ContainsAsync(item);

            if (!exists) return false;

            _context.Entry(item).State = EntityState.Modified;
            return true;
        }
    }
}

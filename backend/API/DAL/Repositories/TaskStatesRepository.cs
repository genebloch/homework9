﻿using DAL.Context;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class TaskStatesRepository : IRepository<Models.TaskState>
    {
        private readonly DataContext _context;

        public TaskStatesRepository() { }
        public TaskStatesRepository(DataContext context)
        {
            _context = context;
        }

        public async Task CreateAsync(Models.TaskState item) => await _context.States.AddAsync(item);

        public async Task<bool> DeleteAsync(int id)
        {
            var item = await _context.States.FirstOrDefaultAsync(s => s.Id == id);
            
            if (item == null) return false;

            _context.States.Remove(item);
            return true;
        }

        public async Task<Models.TaskState> GetAsync(int id) => await _context.States.FirstOrDefaultAsync(s => s.Id == id);
        public virtual async Task<IEnumerable<Models.TaskState>> GetAllAsync() => await _context.States.ToListAsync();

        public async Task<bool> UpdateAsync(Models.TaskState item)
        {
            var exists = await _context.States.ContainsAsync(item);

            if (!exists) return false;

            _context.Entry(item).State = EntityState.Modified;
            return true;
        }
    }
}

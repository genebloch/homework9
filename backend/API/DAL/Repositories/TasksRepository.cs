﻿using DAL.Context;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class TasksRepository : IRepository<Models.Task>
    {
        private readonly DataContext _context;

        public TasksRepository() { }

        public TasksRepository(DataContext context)
        {
            _context = context;
        }

        public async Task CreateAsync(Models.Task item) => await _context.Tasks.AddAsync(item);

        public async Task<bool> DeleteAsync(int id)
        {
            var item = await _context.Tasks.FirstOrDefaultAsync(t => t.Id == id);
            
            if (item == null) return false;

            _context.Tasks.Remove(item);
            return true;
        }

        public async Task<Models.Task> GetAsync(int id) => await _context.Tasks.FirstOrDefaultAsync(t => t.Id == id);
        public virtual async Task<IEnumerable<Models.Task>> GetAllAsync() => await _context.Tasks.ToListAsync();

        public virtual async Task<bool> UpdateAsync(Models.Task item)
        {
            var exists = await _context.Tasks.ContainsAsync(item);

            if (!exists) return false;

            _context.Entry(item).State = EntityState.Modified;
            return true;
        }
    }
}

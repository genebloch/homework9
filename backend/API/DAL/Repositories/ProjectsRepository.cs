﻿using DAL.Context;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class ProjectsRepository : IRepository<Models.Project>
    {
        private readonly DataContext _context;

        public ProjectsRepository() { }

        public ProjectsRepository(DataContext context)
        {
            _context = context;
        }

        public async Task CreateAsync(Models.Project item) => await _context.Projects.AddAsync(item);

        public async Task<bool> DeleteAsync(int id)
        {
            var item = await _context.Projects.FirstOrDefaultAsync(p => p.Id == id);

            if (item == null) return false;

            _context.Projects.Remove(item);
            return true;
        }

        public async Task<Models.Project> GetAsync(int id) => await _context.Projects.FirstOrDefaultAsync(u => u.Id == id);
        public virtual async Task<IEnumerable<Models.Project>> GetAllAsync() => await _context.Projects.ToListAsync();

        public async Task<bool> UpdateAsync(Models.Project item)
        {
            var exists = await _context.Projects.ContainsAsync(item);

            if (!exists) return false;

            _context.Entry(item).State = EntityState.Modified;
            return true;
        }
    }
}

import { Pipe, PipeTransform } from '@angular/core';
  
@Pipe({ name: 'ukrainianDate' })
export class UkrainianDatePipe implements PipeTransform {
  transform(value: string): string {
    const months = ['січня', 'лютого', 'березня', 'квітня', 
                    'травня', 'червня', 'липня', 'серпня', 
                    'вересня', 'жовтня', 'листопада', 'грудня'];
    
    const date = value.split('T')[0].split('-');
    const month = parseInt(date[1]);

    return `${date[2]} ${months[month - 1]} ${date[0]}`;
  }
}
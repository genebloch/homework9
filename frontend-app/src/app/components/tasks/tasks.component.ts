import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/models/task';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ComponentCanDeactivate } from './../../guards/form.guard';
import { Observable } from "rxjs";

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit, ComponentCanDeactivate {

  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder
  ) { 
    this.newEntityForm = this.formBuilder.group({
      name: '',
      description: '',
      createdAt: '',
      finishedAt: '',
      state: '',
      projectId: '',
      performerId: ''
    });
  }

  saved: boolean = false;
  tasks: Task[] = [];
  newEntityForm: any;
  showCreateForm: boolean = false;

  ngOnInit(): void {
    this.http.get(`https://localhost:44354/api/tasks`).subscribe((data: Task[]) => this.tasks = data);
  }

  onSubmit(data: Task) {   
    this.newEntityForm.reset();
    this.saved = true;

    this.http.post('https://localhost:44354/api/tasks', data).subscribe(
      (resp) => {
          if (resp) {
              window.location.reload();
          }
      },
      () => alert('Invalid data'));
  }

  canDeactivate(): boolean | Observable<boolean>{
    if(!this.saved) {
      return confirm("Do you want to leave the page?");
    }
    else {
      return true;
    }
  }

  public createForm() {
    this.showCreateForm = !this.showCreateForm;
  }

  public remove(id: number) {
    this.http.delete(`https://localhost:44354/api/tasks/${id}`).subscribe(() => window.location.reload());
  }
}

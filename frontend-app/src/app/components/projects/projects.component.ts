import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/models/project';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ComponentCanDeactivate } from './../../guards/form.guard';
import { Observable } from "rxjs";

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit, ComponentCanDeactivate {

  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder
  ) { 
    this.newEntityForm = this.formBuilder.group({
      name: '',
      description: '',
      createdAt: '',
      deadline: '',
      authorId: '',
      teamId: ''
    });
  }

  saved: boolean = false;
  projects: Project[] = [];
  newEntityForm: any;
  showCreateForm: boolean = false;

  ngOnInit(): void {
    this.http.get(`https://localhost:44354/api/projects`).subscribe((data: Project[]) => this.projects = data);
  }

  onSubmit(data: Project) {   
    this.newEntityForm.reset();
    this.saved = true;

    this.http.post('https://localhost:44354/api/projects', data).subscribe(
      (resp) => {
          if (resp) {
              window.location.reload();
          }
      },
      () => alert('Invalid data'));
  }

  canDeactivate(): boolean | Observable<boolean>{
    if(!this.saved) {
      return confirm("Do you want to leave the page?");
    }
    else {
      return true;
    }
  }

  public createForm() {
    this.showCreateForm = !this.showCreateForm;
  }

  public remove(id: number) {
    this.http.delete(`https://localhost:44354/api/projects/${id}`).subscribe(() => window.location.reload());
  }
}

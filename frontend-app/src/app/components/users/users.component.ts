import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ComponentCanDeactivate } from './../../guards/form.guard';
import { Observable } from "rxjs";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})

export class UsersComponent implements OnInit, ComponentCanDeactivate {

  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder
  ) { 
    this.newEntityForm = this.formBuilder.group({
      firstName: '',
      lastName: '',
      email: '',
      teamId: '',
      birthday: ''
    });
  }

  saved: boolean = false;
  users: User[] = [];
  newEntityForm: any;
  showCreateForm: boolean = false;

  ngOnInit(): void {
    this.http.get(`https://localhost:44354/api/users`).subscribe((data: User[]) => this.users = data);
  }

  onSubmit(data: User) {   
    this.newEntityForm.reset();
    this.saved = true;

    let date = new Date();
    data.registeredAt = new Date(`${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`);

    this.http.post('https://localhost:44354/api/users', data).subscribe(
      (resp) => {
          if (resp) {
              window.location.reload();
          }
      },
      () => alert('Invalid data'));
  }

  canDeactivate(): boolean | Observable<boolean>{
    if(!this.saved) {
      return confirm("Do you want to leave the page?");
    }
    else {
      return true;
    }
  }

  public createForm() {
    this.showCreateForm = !this.showCreateForm;
  }

  public remove(id: number) {
    this.http.delete(`https://localhost:44354/api/users/${id}`).subscribe(() => window.location.reload());
  }
}

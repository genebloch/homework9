import { Component, OnInit } from '@angular/core';
import { Team } from 'src/app/models/team';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ComponentCanDeactivate } from './../../guards/form.guard';
import { Observable } from "rxjs";

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css']
})

export class TeamsComponent implements OnInit, ComponentCanDeactivate {

  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder
  ) { 
    this.newEntityForm = this.formBuilder.group({
      name: '',
      createdAt: ''
    });
  }

  saved: boolean = false;
  teams: Team[] = [];
  newEntityForm: any;
  showCreateForm: boolean = false;

  ngOnInit(): void {
    this.http.get(`https://localhost:44354/api/teams`).subscribe((data: Team[]) => this.teams = data);
  }

  onSubmit(data: Team) {   
    this.newEntityForm.reset();
    this.saved = true;

    this.http.post('https://localhost:44354/api/teams', data).subscribe(
      (resp) => {
          if (resp) {
              window.location.reload();
          }
      },
      () => alert('Invalid data'));
  }

  canDeactivate(): boolean | Observable<boolean>{
    if(!this.saved) {
      return confirm("Do you want to leave the page?");
    }
    else {
      return true;
    }
  }

  public createForm() {
    this.showCreateForm = !this.showCreateForm;
  }

  public remove(id: number) {
    this.http.delete(`https://localhost:44354/api/teams/${id}`).subscribe(() => window.location.reload());
  }
}

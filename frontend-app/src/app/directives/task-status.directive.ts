import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appTaskStatus]'
})
export class TaskStatusDirective implements OnInit {
  @Input() status: string;

  element: ElementRef

  constructor(element: ElementRef) {
    this.element = element;
  }

  ngOnInit() {
    this.setColor();
  }

  private setColor() {
    if (this.status === '3') {
      this.element.nativeElement.style.backgroundColor = '#5eff89';
    }
  }
}
